#Bastien Lemarchand
#Sébastien Goffart

#----------------PROJET : Quels seront vos résultats aux prochains examens? ----------------#

# ---------- Requirement ---------- # 

import numpy as np 
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib.pyplot as plt #graph
from sklearn.metrics import mean_squared_error #model's evaluation
from sklearn.metrics import accuracy_score
from sklearn.metrics import r2_score
from sklearn.model_selection import cross_val_score

###Pour installer seaborn : pip install seaborn###
import seaborn as sns

# ---------- Data Loading ---------- # 

por = pd.read_csv("./student-por.csv")
print('table portuguais; \n',por.head(), ' // nb etudiants = ', len(por),' // colonnes = ', por.columns)

# --------- Data Cleaning ---------- #
#math.rename(columns={'G1':'G1_Mat', 'G2':'G2_Mat', 'G3':'G3_Mat'}, inplace=True) # renommage des notes pour les trimestres de math
por.rename(columns={'G1':'G1_por', 'G2':'G2_por', 'G3':'G3_por'}, inplace=True) # renommage des notes pour les trimestres de portuguais
por['Mean_Grades'] = round((por['G1_por'] + por['G2_por'] + por['G3_por'])/3,1)
#print(por['Mean_Grades'])


#Verifie si valeurs manquantes
#print("Nombre de valeurs manquantes : ",por.isnull().sum())

# ---------- data analysis ---------- #

##print("Analyse descriptive \n")
##
###Proportion filles/garçons
##print("percentage total female : ",(por["sex"] == 'F').value_counts(normalize = True)[1]*100)
##print("percentage total male : ",(por["sex"] == 'M').value_counts(normalize = True)[1]*100)
##print("\n")
###sns.countplot(por.sex)
###sns.plt.show()
##
###Proportion d'eleves dans les 2 écoles
###sns.countplot(por.school)
###sns.plt.show()
##
###Influence de l'alcool sur les notes
##sns.violinplot( x=por["Dalc"], y=por['Mean_Grades'], linewidth=5)
###sns.plt.show()
##
###Correlation entre les variables
##cor=por.corr()
##f,ax=plt.subplots()
##sns.heatmap(cor,annot=True,linewidth=0.5,fmt='.3f',ax=ax)
##plt.show()
##print("Pas bcp de corrélation... le redoublement, le temps de travail et le niveau d'education des parents sont les variables les plus correles aux notes + les notes aux examens précédents")

# ---------- modèle de prédiction ---------- #

# NB il faut penser a faire un genre de controle que l'on puisse appliquer sur toutes les 
# méthodes de prédictions un peu comme un outils de mesure de la validiter de nos tests ou 
# bien voir si ça existe pas déjà dans sklearn

#construction de données:
por['school'] = por['school'].map({'GP': 0, 'MS': 1}).astype(int)
por['sex'] = por['sex'].map({'M': 0, 'F': 1}).astype(int)
por['address'] = por['address'].map({'R': 0, 'U': 1}).astype(int)
por['famsize'] = por['famsize'].map({'LE3': 0, 'GT3': 1}).astype(int)
por['Pstatus'] = por['Pstatus'].map({'A': 0, 'T': 1}).astype(int)
por['Mjob'] = por['Mjob'].map({'at_home': 0, 'health': 1, 'other': 2, 'services': 3, 'teacher': 4}).astype(int)
por['Fjob'] = por['Fjob'].map({'at_home': 0, 'health': 1, 'other': 2, 'services': 3, 'teacher': 4}).astype(int)
por['reason'] = por['reason'].map({'course': 0, 'other': 1, 'home': 2, 'reputation': 3}).astype(int)
por['guardian'] = por['guardian'].map({'mother': 0, 'father': 1, 'other': 2}).astype(int)
por['schoolsup'] = por['schoolsup'].map({'no': 0, 'yes': 1}).astype(int)
por['famsup'] = por['famsup'].map({'no': 0, 'yes': 1}).astype(int)
por['paid'] = por['paid'].map({'no': 0, 'yes': 1}).astype(int)
por['activities'] = por['activities'].map({'no': 0, 'yes': 1}).astype(int)
por['nursery'] = por['nursery'].map({'no': 0, 'yes': 1}).astype(int)
por['higher'] = por['higher'].map({'no': 0, 'yes': 1}).astype(int)
por['internet'] = por['internet'].map({'no': 0, 'yes': 1}).astype(int)
por['romantic'] = por['romantic'].map({'no': 0, 'yes': 1}).astype(int)

from sklearn.model_selection import train_test_split
IndicateursFrame = por.drop(columns=['G1_por','G2_por','G3_por','Mean_Grades']) # on recupère tout le dataframe sauf la note G3_por
ResultatsFrame = por['Mean_Grades'] #on construit un dataframe avec uniquement la note G3


X_train, X_test, Y_train, Y_test=train_test_split(IndicateursFrame,ResultatsFrame,test_size=0.30)
#print('table X_train; \n',X_train.head(), ' // nb etudiants = ', len(X_train),' // colonnes = ', X_train.columns)
#print('table Y_train; \n',Y_train.head(), ' // nb etudiants = ', len(Y_train))
#print('longeur Y_train',len(Y_train))


# ---- Classification supervisée (Arbre de Décisions + Régression Linéaire + SVM + Reseau de Neurones) ---- #

# ---- Decision Tree ---- #

# requirement:  
from sklearn import tree
from graphviz import *
from io import StringIO
import pydotplus

#test pour graphviz
# g=Digraph('G', filename='hello.gv')
# g.edge('Hello','World')
# g.view()

##clf_tree = tree.DecisionTreeRegressor()
##clf_tree = clf_tree.fit(X_train, Y_train)
##pred_tree = clf_tree.predict(X_test)
##
###Exporter les résultats dans un fichier dot + representation graphique: 
##dot_data = StringIO()
##tree.export_graphviz(clf_tree, out_file=dot_data, filled = True, feature_names = X_train.columns)
##with open("Decision_tree_regressor.dot", 'w') as f:
##    f.write(dot_data.getvalue())
##graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
##graph.write_pdf("Figure_decision_tree_regressor.pdf")

# # ---- Regression Lineaire Multiples ---- #

# requirement:
import statsmodels.api as sm

regressor_OLS=sm.OLS(endog = Y_train, exog = X_train).fit()
print(regressor_OLS.summary())
predictions=regressor_OLS.predict(X_test)
print(np.sqrt(mean_squared_error(Y_test,predictions)))



# # ---- Neural Network ---- #
# #Neural Network : PENSER A FAIRE VARIER NOMBRES DE COUCHES ET NOMBRES DE NEURONES PAR COUCHES
# # requirement:
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import GridSearchCV
import itertools

##for i in range(100,1000,100) :
##    a=tuple()
##    for j in range (10,30,5) :
##        a=a+(i,)
##        mlpR=MLPRegressor(hidden_layer_sizes=a,max_iter=100000)
##        mlpR.fit(X_train,Y_train)
##        predictions=mlpR.predict(X_test)
##        nberr=0
##        print(np.sqrt(mean_squared_error(Y_test,predictions)))


mlpregressor = MLPRegressor(random_state=1, max_iter=1000000)
                           
                           
check_parameters = {
    'hidden_layer_sizes': [x for x in itertools.product(range(1,400,100), range(1,10,2))],
    'activation': ['tanh', 'relu'],
}

gridsearchcv = GridSearchCV(mlpregressor, check_parameters,scoring='neg_mean_squared_error', n_jobs=-1, cv=3)
gridsearchcv.fit(X_train, Y_train)
predictions=gridsearchcv.predict(X_test)
print(mean_squared_error(Y_test,predictions))
print('Best parameters found:\n', gridsearchcv.best_params_)

#import itertools
#print(x for x in itertools.product(range(1,100), range(1,10)))
# #---- SVM ---- #

# # requirement:
from sklearn import svm
from sklearn.svm import LinearSVC
from sklearn.svm import SVR



##params= {
##    'kernel':['rbf','poly','linear'], #linear
##    'C':[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9],
##    'epsilon':[0.01,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,2.0,3.0,1.1,1.2,1.3,1.4],
##    'gamma':[0.1,0.2,0.3,0.4,0.5]
##        }
##
##reg_svr = SVR()
##gs = GridSearchCV(reg_svr,params, cv=5, n_jobs=-1) 
##gs.fit (X_train, Y_train)
##reg_svr=gs.best_estimator_
##print(reg_svr)


#SVR(C=0.1, epsilon=1.4, gamma=0.1, kernel='linear')
                                                                                                      










