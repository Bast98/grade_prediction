#Bastien Lemarchand
#Sébastien Goffart
#GB5 - 01/2021

#----------------PROJET : Prédiction de notes aux examens et comparaison de méthodes ----------------#

# ---------- 1.Requirement ---------- # 

#data managment
import numpy as np 
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib.pyplot as plt #graph
import seaborn as sns

#split dataset
from sklearn.model_selection import train_test_split

#optimiser les hyperparamètres 
from sklearn.model_selection import GridSearchCV # fonction qui permet une optimisation des paramètres par validation croisée à k blocs

# Decision tree:
from sklearn import tree
from graphviz import *
from io import StringIO
import pydotplus

# regressions linéaires multiples
import statsmodels.api as sm
from sklearn.linear_model import LinearRegression
from sklearn.feature_selection import RFE
from sklearn import preprocessing

# neural network
from sklearn.neural_network import MLPRegressor
import itertools

# SVM
from sklearn import svm
from sklearn.svm import LinearSVC
from sklearn.svm import SVR

# paramètres métriques 
from sklearn.metrics import mean_squared_error #model's evaluation
from sklearn.metrics import accuracy_score
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import explained_variance_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import plot_confusion_matrix
from sklearn.model_selection import cross_val_score

# ---------- 2.Data Loading ---------- # 

#math = pd.read_csv("./student-mat.csv")
por = pd.read_csv("./student-por.csv")
print('table portuguais; \n',por.head(), ' // nb etudiants = ', len(por),' // colonnes = ', por.columns)

# --------- 3.Data Cleaning ---------- #
#math.rename(columns={'G1':'G1_Mat', 'G2':'G2_Mat', 'G3':'G3_Mat'}, inplace=True) # renommage des notes pour les trimestres de math
por.rename(columns={'G1':'G1_por', 'G2':'G2_por', 'G3':'G3_por'}, inplace=True) # renommage des notes pour les trimestres de portuguais
por['Mean_Grades'] = round((por['G1_por'] + por['G2_por'] + por['G3_por'])/3,1)
#print(por['Mean_Grades'])

# ---------- 4.Data analysis ---------- #

print("Analyse descriptive \n")

#les variables Binaires: 
binary_variables= ['school', 'sex','address', 'famsize', 'Pstatus','schoolsup', 'famsup', 
                    'paid', 'activities', 'nursery', 'higher', 'internet', 'romantic']
#les variables numériques: 
num_variables= ['age','Medu','Fedu','traveltime', 'studytime','failures', 
                'famrel','freetime', 'goout', 'Dalc','Walc', 'health', 'absences', 
                'G1_por','G2_por','G3_por','Mean_Grades']
#les variables nominales: 
nom_variables= ['Mjob', 'Fjob', 'reason', 'guardian']

def disp_figure_varia(list_varia,n_figline,n_figcol):
    #//permet de representer le nombre d'etudiant en fonction d'une liste de paramètres, le nombre de ligne (=n_figline)
    #  un nombre de figure par ligne (=n_figcol)
    plt.figure(figsize=[15,17])
    n=1
    for f in list_varia:
        plt.subplot(n_figline,n_figcol,n)
        sns.countplot(x=f, edgecolor="black", data=por)
        sns.despine()
        plt.title("Répartion par '{}'".format(f))
        plt.ylabel("nombre d'étudiants")
        n=n+1
    plt.tight_layout()
    plt.show()
    return()

disp_figure_varia(binary_variables,4,4)
disp_figure_varia(num_variables,5,4)
disp_figure_varia(nom_variables,1,4)

#Proportion filles/garçons
print("percentage total female : ",(por["sex"] == 'F').value_counts(normalize = True)[1]*100)
print("percentage total male : ",(por["sex"] == 'M').value_counts(normalize = True)[1]*100)
print("\n")
#sns.countplot(por.sex)
#sns.plt.show()

#Influence de l'alcool sur les notes
sns.violinplot( x=por["Dalc"], y=por['Mean_Grades'], linewidth=5)
#sns.plt.show()

#Correlation entre les variables 
cor=por.corr()
f,ax=plt.subplots()
sns.heatmap(cor,annot=True,linewidth=0.5,fmt='.3f',ax=ax)
plt.show()


# ---------- 5.Adaptation des données ---------- #

# Certaines variables sont des chaines de caractères et nécessite d'être 
# converties en variables de type numériques pour pouvoir construire les 
# modèles

#construction de données:
por['school'] = por['school'].map({'GP': 0, 'MS': 1}).astype(int)
por['sex'] = por['sex'].map({'M': 0, 'F': 1}).astype(int)
por['address'] = por['address'].map({'R': 0, 'U': 1}).astype(int)
por['famsize'] = por['famsize'].map({'LE3': 0, 'GT3': 1}).astype(int)
por['Pstatus'] = por['Pstatus'].map({'A': 0, 'T': 1}).astype(int)
por['Mjob'] = por['Mjob'].map({'at_home': 0, 'health': 1, 'other': 2, 'services': 3, 'teacher': 4}).astype(int)
por['Fjob'] = por['Fjob'].map({'at_home': 0, 'health': 1, 'other': 2, 'services': 3, 'teacher': 4}).astype(int)
por['reason'] = por['reason'].map({'course': 0, 'other': 1, 'home': 2, 'reputation': 3}).astype(int)
por['guardian'] = por['guardian'].map({'mother': 0, 'father': 1, 'other': 2}).astype(int)
por['schoolsup'] = por['schoolsup'].map({'no': 0, 'yes': 1}).astype(int)
por['famsup'] = por['famsup'].map({'no': 0, 'yes': 1}).astype(int)
por['paid'] = por['paid'].map({'no': 0, 'yes': 1}).astype(int)
por['activities'] = por['activities'].map({'no': 0, 'yes': 1}).astype(int)
por['nursery'] = por['nursery'].map({'no': 0, 'yes': 1}).astype(int)
por['higher'] = por['higher'].map({'no': 0, 'yes': 1}).astype(int)
por['internet'] = por['internet'].map({'no': 0, 'yes': 1}).astype(int)
por['romantic'] = por['romantic'].map({'no': 0, 'yes': 1}).astype(int)

IndicateursFrame = por.drop(columns=['G1_por','G2_por','G3_por','Mean_Grades']) # on recupère tout le dataframe à l'exception des notes
ResultatsFrame = por['Mean_Grades'] #on construit un dataframe avec uniquement la note moyennes Mean_Grades

X_train, X_test, Y_train, Y_test=train_test_split(IndicateursFrame,ResultatsFrame,test_size=0.30, random_state = 20)
# print('table X_train; \n',X_train.head(), ' // nb etudiants = ', len(X_train),' // colonnes = ', X_train.columns)
# print('table Y_train; \n',Y_train.head(), ' // nb etudiants = ', len(Y_train))
# print('longeur Y_train',len(Y_train))

# ---- 6.Classification supervisée (Arbre de Décisions + Régression Linéaire + SVM + Reseau de Neurones) ---- #

# évaluation des modèles par validation externe:

def eval_model(Yt,predict):
    #renvoie les valeurs de MAE et de R² pour le modèle évalué
    result=dict()
    result["MAE"]=mean_absolute_error(Yt,predict)
    # result["explained_variance_score"] = explained_variance_score(Yt,predict)
    result["r2"] = r2_score(Yt,predict)
    return(result)

# ---- 6.1.Decision Tree ---- #

print("ARBRE DE DECISION (REGRESSION) :")
params_tree= {
    'max_depth':range(1,50),
    'min_samples_leaf':range(1,10)
    }

#__Optimisation__
# clf_tree = tree.DecisionTreeRegressor()
# gs_tree = GridSearchCV(clf_tree,params_tree, cv=5, n_jobs=-1, scoring = 'neg_mean_absolute_error') 
# gs_tree.fit (X_train, Y_train)
# clf_tree_opt=gs_tree.best_estimator_
# print(clf_tree_opt)
# print(gs_tree.best_score_)

#construction du modèle optimisé:
clf_tree = tree.DecisionTreeRegressor(max_depth=3, min_samples_leaf=7)
clf_tree = clf_tree.fit(X_train, Y_train)
pred_tree = clf_tree.predict(X_test)
print("évaluation modèle:",eval_model(Y_test,pred_tree))

# #Exporter les résultats dans un fichier dot + representation graphique: 
# dot_data = StringIO()
# tree.export_graphviz(clf_tree, out_file=dot_data, filled = True, feature_names = X_train.columns)
# with open("Decision_tree_regressor.dot", 'w') as f:
#    f.write(dot_data.getvalue())
# graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
# graph.write_pdf("Figure_decision_tree_regressor.pdf")

# ---- 6.2.Regression Lineaire Multiples ---- #
#Normalization & Standardization
min_max_scaler = preprocessing.MinMaxScaler()
por_scaled=min_max_scaler.fit_transform(por)
IndicateursFrame_scaled = por.drop(columns=['G1_por','G2_por','G3_por','Mean_Grades']) # on recupère tout le dataframe sauf la note G3_por
ResultatsFrame_scaled = por['Mean_Grades']
X_train_scaled, X_test_scaled, Y_train_scaled, Y_test_scaled=train_test_split(IndicateursFrame,ResultatsFrame,test_size=0.30, random_state = 20)

print("REGRESSION LINEAIRE :")
params = [{'n_features_to_select': list(range(1, 30))}]

#__Optimisation__
##lm = LinearRegression()
##rfe = RFE(lm)
##gridsearchcv = GridSearchCV(rfe, param_grid = params, scoring= 'neg_mean_absolute_error', cv = 5, n_jobs=-1)
##gridsearchcv.fit(X_train_scaled, Y_train_scaled)
##predictions=gridsearchcv.predict(X_test_scaled)
##print('Meilleur paramètre trouvé:\n', gridsearchcv.best_params_)
##print('Meilleur estimateur',gridsearchcv.best_estimator_)
##print('Erreur moyenne absolue grid:\n',gridsearchcv.best_score_)
##print('Erreur moyenne absolue prediction:\n',mean_absolute_error(Y_test_scaled,predictions))

#construction du modèle optimisé:
rfe=RFE(estimator=LinearRegression(), n_features_to_select=10)
rfe.fit(X_train_scaled,Y_train_scaled)
print(rfe.estimator_.intercept_)
n=0
for i in range(X_train_scaled.shape[1]):
    if rfe.support_[i]==True :
        print('Column: ',list(X_train)[i],' Coef: ',rfe.estimator_.coef_[n])
        n=n+1
pred_RFE=rfe.predict(X_test_scaled)
print(eval_model(Y_test_scaled,pred_RFE))

# # ---- 6.3.Neural Network ---- #
#  PENSER A FAIRE VARIER NOMBRES DE COUCHES ET NOMBRES DE NEURONES PAR COUCHES
print("RESEAU NEURAL: ")

check_parameters = {
   'hidden_layer_sizes': [x for x in itertools.product(range(100,400,50), range(0,10,2))],
   'activation': ['tanh', 'relu'],
}
#__Optimisation__
##mlpregressor = MLPRegressor(random_state=1, max_iter=1000000)
##gridsearchcv = GridSearchCV(mlpregressor, check_parameters,scoring='neg_mean_absolute_error', n_jobs=-1, cv=5)
##gridsearchcv.fit(X_train, Y_train)
##predictions=gridsearchcv.predict(X_test)
##print('Meilleur paramètre trouvé:\n', gridsearchcv.best_params_)
##print(gridsearchcv.best_estimator_)
##print('Erreur moyenne absolue grid:\n',gridsearchcv.best_score_)
##print('Erreur moyenne absolue prediction:\n',mean_absolute_error(Y_test,predictions))

#construction du modèle optimisé:
mlpregressor=MLPRegressor(activation='tanh', hidden_layer_sizes=(200,4), max_iter=1000000,random_state=1)
mlpregressor.fit(X_train, Y_train)
pred_NN=mlpregressor.predict(X_test)
print(eval_model(Y_test,pred_NN))

#---- SVM ---- #
print("SVM:")

params= {
   'kernel':['rbf','poly','linear'],
   'C':[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9],
   'epsilon':[0.01,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,2.0,3.0,1.1,1.2,1.3,1.4],
   'gamma':[0.1,0.2,0.3,0.4,0.5]
       }

#__Optimisation__
##reg_svr = SVR()
##gridsearchcv = GridSearchCV(reg_svr,params,scoring='neg_mean_absolute_error', cv=5, n_jobs=-1) 
##gridsearchcv.fit(X_train, Y_train)
##reg_svr=gridsearchcv.best_estimator_
##predictions=gridsearchcv.predict(X_test)
##print('Meilleur paramètre trouvé :\n', gridsearchcv.best_params_)
##print(reg_svr)
##print('Erreur moyenne absolue grid:\n',gridsearchcv.best_score_)
##print('Erreur moyenne absolue prediction:\n',mean_absolute_error(Y_test,predictions))

#construction du modèle optimisé:
reg_svr=SVR(C=0.3, epsilon=0.01, gamma=0.1, kernel='linear')
reg_svr.fit(X_train, Y_train)
pred_SVR=reg_svr.predict(X_test)
print(eval_model(Y_test,pred_SVR))

# ------------------------Classification (pass or not)--------------------#
bins = (-1, 9.5, 21)
performance_level = ['fail', 'pass']
ResultatsFrame = pd.cut(por['Mean_Grades'], bins = bins, labels = performance_level)

X_train, X_test, Y_train, Y_test=train_test_split(IndicateursFrame,ResultatsFrame,test_size=0.30, random_state = 20)

# --------DECISION TREE---------#
print("ARBRE DE DECISION (CLASSIFICATION) :")

tree_param = [{'max_depth': range(1,50),
              'min_samples_leaf': range(1,10)}]

##DT=tree.DecisionTreeClassifier()
##grid=GridSearchCV(DT,tree_param,cv=10,n_jobs=-1)
##grid.fit(X_train,Y_train)
##print(grid.best_params_)
##print(grid.best_estimator_)
##print(grid.best_score_)

#construction du modèle optimisé:
DT=tree.DecisionTreeClassifier(criterion='entropy', max_depth=5, min_samples_leaf=9)
DT.fit(X_train,Y_train)
pred=DT.predict(X_test)
print(accuracy_score(Y_test,pred))
plot_confusion_matrix(DT, X_test, Y_test)  
plt.show() 
                                                  
#Exporter les résultats dans un fichier dot + representation graphique: 
#dot_data = StringIO()
#tree.export_graphviz(DT, out_file=dot_data, filled = True, feature_names = X_train.columns)
#with open("Decision_tree_classifier.dot", 'w') as f:
#    f.write(dot_data.getvalue())
#graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
#graph.write_pdf("Figure_decision_tree_classifier.pdf")                                                                                             










